'use strict';

$(document).ready(function(){
	var elHeight = $('#home').height();

	$(window).on('scroll', function(e){
		console.log(window.scrollY >= elHeight && !$('#menu').hasClass('navbar-shrink'));
		if( window.scrollY >= elHeight){
			$('#menu').addClass('navbar-shrink');
		} else {
			$('#menu').removeClass('navbar-shrink');
		}
	});

	// Google Maps API
	// Definindo uma chave diferente para cada acesso, mobile ou desktop
	
	var APIkey = (function(){
		var isAndroid = (/armv/).test( window.navigator.platform );

		return isAndroid ?
			"AIzaSyB_Im8QJE25qpMS-ab8SmapTuPq8tRJF2I" :
			"AIzaSyDJNgFCK33MzKn4596TCJOVxGj1dyXjWQw" ;
	})();

	// Pegando script da API
	$.getScript('http://maps.googleapis.com/maps/api/js?key=' + APIkey + '&sensor=false&callback=googleMapsInit');
	
	// Autocomplete	
	// Array com categorias presentes na cidade
	var categories = [
		{ value: 'Sorveterias', data:1 },
		{ value: 'Restaurantes', data:2 },
		{ value: 'Hotéis', data:3 },
		{ value: 'Escolas', data:4 },
		{ value: 'Testes', data:5 }
	];
	// Plugin jQuery
	$('#home .search .form-control').autocomplete({
		lookup: categories,
		onSelect: function (suggestion) {
			console.log('You selected: ' + suggestion.value + ', ' + suggestion.data);
		}
	});

});

// Função chamada após carregamento da API
function googleMapsInit(){
	// Pegando objeto global
	var google = window.google;
	// Definindo node do mapa
	var mapCanvas = $("#map_canvas").get(0),
		// Definição dos estilos do mapa
		// JSON Gerado por: http://gmaps-samples-v3.googlecode.com/svn/trunk/styledmaps/wizard/index.html
		mapStyle = [{"featureType": "road.local","elementType": "geometry.fill","stylers": [{ "color": "#9bbad7" },{ "lightness": -23 }]},{"featureType": "road.highway","stylers": [{ "color": "#8ba7c6" },{ "lightness": -19 }]},{"featureType": "landscape.man_made","stylers": [{ "color": "#1d82d6" },{ "lightness": 87 }]},{"featureType": "road.local","elementType": "geometry","stylers": [{ "visibility": "simplified" }]},{"featureType": "road.local","elementType": "labels","stylers": [{ "lightness": 100 },{ "weight": 0.4 }]},{"featureType": "landscape","elementType": "geometry.fill","stylers": [{ "color": "#6dadd0" }]},{"featureType": "poi","elementType": "geometry.fill","stylers": [{ "lightness": 9 },{ "color": "#33b7cc" }]}],
		// Criando tipo de mapa estilizado
		styledMap = new google.maps.StyledMapType( mapStyle, { name: "Styled Map" }),
		// Opções do mapa
		mapOptions = {
			// Distância
			zoom: 17,
			// Aqui fica a localozação da cidade
			center: new google.maps.LatLng(-5.895419, -42.634478),
			// Deixando os berimbelos ao redor do mapa invisíveis
			mapTypeControl: false,
			zoomControl: false,
			overviewMapControl: false,
			scaleControl: false,
			// Definindo mapa como estilizado
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
			}
		},
		// Criando mapa
		map = new google.maps.Map( mapCanvas, mapOptions);
	
	// Aplicando estilo ao mapa
	map.mapTypes.set('map_style', styledMap);
	map.setMapTypeId('map_style');
};